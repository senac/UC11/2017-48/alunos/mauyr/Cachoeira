package com.example.sala304b.cachoeira;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sala304b.cachoeira.Cachoeira;

import java.util.ArrayList;
import java.util.List;

public class BancodeDadosCachoira extends SQLiteOpenHelper {

    private static final String DATABASE = "Cachoeira";
    private static final int VERSAO = 1;

    /**/
    public BancodeDadosCachoira(Context context) {
        super(context, DATABASE, null, VERSAO);
    }


    public BancodeDadosCachoira(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String dd1 = "CREATE TABLE Cachoeira( " +
                "id INTEGER PRIMARY KEY, " +
                "nome TEXT NOT NULL, " +
                "informacoes TEXT, " +
                "telefone TEXT, " +
                "email TEXT, " +
                "endereco TEXT, " +
                "classificacao REAL, " +
                "site TEXT );";
        db.execSQL(dd1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String dd1 = "DROP TABLE IF EXISTS Cachoeira;";
        db.execSQL(dd1);
        this.onCreate(db);
    }

    public void salvar(Cachoeira cachoeira) {
        ContentValues values = new ContentValues();
        values.put("nome", cachoeira.getNome());
        values.put("informacoes", cachoeira.getInformocoes());
        values.put("email", cachoeira.getEmail());
        values.put("classificacao", cachoeira.getClassificacao());
        values.put("telefone", cachoeira.getTelefone());
        values.put("site", cachoeira.getSite());

        getWritableDatabase().insert("Cachoeira", null, values);
    }

    public List<Cachoeira> getLista() {
        List<Cachoeira> lista = new ArrayList<>();
        String colunas[] = {"id", "nome", "informacoes", "telefone", "email", "endereco", "classificacao", "site"};
        Cursor cursor = getWritableDatabase().query(
                "Cachoeira", colunas, null, null, null, null, null);


        while (cursor.moveToNext()) {
            Cachoeira cachoeira = new Cachoeira();
            cachoeira.setId(cursor.getInt(0));
            cachoeira.setNome(cursor.getString(1));
            cachoeira.setInformocoes(cursor.getString(2));
            cachoeira.setTelefone(cursor.getString(3));
            cachoeira.setEmail(cursor.getString(4));
            cachoeira.setEndereco(cursor.getString(5));
            cachoeira.setClassificacao(cursor.getFloat(6));
            cachoeira.setSite(cursor.getString(7));


            lista.add(cachoeira);
        }
        return lista;
    }
}
